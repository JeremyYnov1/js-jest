function absolute(number) {
    return (number < 0) ? -number : number;
}

export { absolute };