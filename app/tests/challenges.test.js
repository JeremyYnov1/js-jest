import { absolute } from '../js/challenges';

it('absolute of -7 is 7', () => {
    expect(absolute(-7)).toBe(7);
});

it('absolute of 13 is 13', () => {
    expect(absolute(13)).toBe(13);
});